const express = require("express")
const bodyParser = require("body-parser")
const fs = require("fs")

const stylus = require('stylus')
const nib = require('nib')

const app = express()
const port = process.env.PORT || 8080;
const router = express.Router()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib());
}
app.set('view engine', 'jade')

app.use(stylus.middleware(
  { src: __dirname + '/public'
  , compile: compile
  }
));
app.use(express.static(__dirname + '/public'))

router.get('/', (req, res) => {
  res.render("stardust.jade")
})

router.route('/ids').get((req,res) => {
  fs.readFile("./ID.json", "utf-8", (err, data) => {
    if(err) throw err
    res.json(JSON.parse(data))
  })
})

router.route("/ids/nick/:nickname").get((req,res) => {
  let nickname = req.params.nickname
  fs.readFile("./ID.json", "utf-8", (err, data) => {
    if(err) throw err
    data = JSON.parse(data)
    res.json(data[nickname])
  })
})

router.route("/ids/search/:param/:value").get((req, res) => {
  let param = req.params.param
  let value = req.params.value

  fs.readFile("./ID.json", "utf-8", (err, data) => {
    if(err) throw err
    data = JSON.parse(data)

    let results = []

    for(each in data) {
      let element = data[each]

      if(element[param]) {
        if(element[param].includes(value)) {
          element.nick = each
          results.push(element)
        }
      }
    }

    res.json(results)

  })
})

app.use('/stardust', router)

// this is only for the corile.ga frontend
app.get('/', (req, res) => {
  res.render("index.jade")
})


app.listen(port)
