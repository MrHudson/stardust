function $(q) {
  return document.querySelector(q)
}

let data = []

fetch("/stardust/ids")
.then((res) => res.json())
.then((val) => {
  data = val

  let names = []

  for(el in data) {
    let option = document.createElement("option")
    option.text = data[el].name
    option.value = el
    //Note to self: text and value are different parameters you fucking moron.
    $("#idselect").appendChild(option)
  }

  let url = location.href.split("#")
  if(url[1]) change()

})

$("#idselect").addEventListener("change", change)

function change() {

  let url = location.href
  url = url.split("#")

  if($("#idselect").value) {
    url[1] = $("#idselect").value
  }

  let nickname = url[1]
  let key = data[url[1]]

  window.location.href = url.join("#")

  $("#faction-name").textContent = key.name
  $("#nickname").textContent = nickname
  $("#nickname-link").href = "/stardust/ids/nick/"+nickname
  $("#description").textContent = key.desc
  $("#zoi").textContent = key.zoi

  while($("#lines").hasChildNodes()) {
    $("#lines").removeChild($("#lines").lastChild)
  }

  for(el of key.lines) {
    let line = document.createElement("li")
    line.textContent = el
    $("#lines").appendChild(line)
  }

  for(let i=0; i < $("#ships").children.length; i++) {
    $("#ships").children[i].classList.remove("active")
  }

  for(el of key.ships) {
    if(el=="fighter") $("li#fighter").classList.add("active")
    else if(el=="freighter") $("li#freighter").classList.add("active")
    else if(el=="transport") $("li#transport").classList.add("active")
    else if(el=="gunboat") $("li#gunboat").classList.add("active")
    else if(el=="cruiser") $("li#cruiser").classList.add("active")
    else if(el=="battleship") $("li#battleship").classList.add("active")
    else if(el=="kusari destroyer") $("li#kudessie").classList.add("active")
    else if(el=="nomad morph") $("li#morph").classList.add("active")
    else if(el=="nomad fighter") $("li#nfighter").classList.add("active")
    else if(el=="nomad gunboat") $("li#ngunboat").classList.add("active")
  }

}
